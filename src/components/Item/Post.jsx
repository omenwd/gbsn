import React, { Component } from "react";
import classNames from "classnames";
import moment from "moment";

class Post extends Component {
  state = {};

  convertIndicator = (indicator) => (indicator >= 1000
    ? `${(indicator / 1000).toFixed(0)}k`
    : indicator
  );

  // eslint-disable-next-line camelcase
  indicatorsFB = ({ comments_count, likes_count, shares_count }) => (
    <div className="ptp-footer d-flex">
      <div className="ptp-f-container d-flex align-items-center">
        <div className="ptp-f-icon">
          <i className="pr-icon-like" />
        </div>
        <div className="ptp-f-text">
          {this.convertIndicator(likes_count)}
        </div>
      </div>
      <div className="ptp-f-container d-flex align-items-center">
        <div className="ptp-f-icon">
          <i className="pr-icon-comment" />
        </div>
        <div className="ptp-f-text">
          {this.convertIndicator(comments_count)}
        </div>
      </div>
      <div className="ptp-f-container d-flex align-items-center">
        <div className="ptp-f-icon">
          <i className="pr-icon-share" />
        </div>
        <div className="ptp-f-text">
          {this.convertIndicator(shares_count)}
        </div>
      </div>
    </div>
  );

  // eslint-disable-next-line camelcase
  indicatorsInsta = ({ comments_count, likes_count, shares_count }) => (
    <div className="ptp-footer d-flex">
      <div className="ptp-f-container d-flex align-items-center">
        <div className="ptp-f-icon">
          <i className="pr-icon-heart" />
        </div>
        <div className="ptp-f-text">
          {this.convertIndicator(likes_count)}
        </div>
      </div>
      <div className="ptp-f-container d-flex align-items-center">
        <div className="ptp-f-icon">
          <i className="pr-icon-comment" />
        </div>
        <div className="ptp-f-text">
          {this.convertIndicator(comments_count)}
        </div>
      </div>
      <div className="ptp-f-container d-flex align-items-center">
        <div className="ptp-f-icon">
          <i className="pr-icon-share" />
        </div>
        <div className="ptp-f-text">
          {this.convertIndicator(shares_count)}
        </div>
      </div>
    </div>
  );

  render() {
    const {
      data: {
        // eslint-disable-next-line camelcase
        user, post_content, post_metrics, social, post_created_at,
      }, data,
    } = this.props;
    const { [social]: socNetworkObject } = data;
    return (
      <div className="col col-12 col-md-7 mb-2">
        <a href={socNetworkObject[0].accountinfo.url} className="no-style" target="_blank" rel="noopener noreferrer">
          <div className="profile-timeline-post pb-2 mb-0 prestigio-shadow">
            <div className="ptp-header mb-2 d-flex justify-content-between relative">
              <div className="ptp-header-left d-flex">
                <div className="prestigio-thumbnail" style={{ backgroundImage: `url(${user[0].userinfo.avatar_url})` }} />
                <div className="ptp-header-left-text ml-2 d-flex flex-column justify-content-between">
                  <div className="ptp-header-name">
                    {user[0].userinfo.displayname}
                  </div>
                  <div className="ptp-header-info">
                    <span className={classNames("ptp-h-sn", social)} style={{ textTransform: "capitalize" }}>{social}</span>
                    <span className="ptp-h-sep">.</span>
                    <span className="ptp-h-date">
                      {moment(post_created_at).format("MMM D YYYY HH:MM")}
                    </span>
                    <span className="ptp-h-sep">.</span>
                    <span className="ptp-h-prv"><i className="pr-icon-world" /></span>
                  </div>
                </div>
              </div>
            </div>
            <div className="ptp-content mb-2">
              <div className="ptp-c-text">
                <p className="mb-0">
                  {post_content.text}
                </p>
              </div>
              <div className="ptp-c-thumb">
                <img alt={post_content.text} src={post_content.picture} />
              </div>
            </div>
            {social === "facebook" ? this.indicatorsFB(post_metrics) : this.indicatorsInsta(post_metrics)}
          </div>
        </a>
      </div>
    );
  }
}

export default Post;
