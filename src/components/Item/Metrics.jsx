import React, { Component } from "react";
import { cloneDeep } from "lodash";

class Metrics extends Component {
  taggedBrands = (mentions) => {
    const { showModal } = this.props;
    const displayMentions = cloneDeep(mentions);
    let addonMentionsCount = 0;
    if (mentions.length > 2) {
      addonMentionsCount = mentions.length - 2;
      displayMentions.splice(2, displayMentions.length - 2);
    }
    return (
      <>
        <div className="small-title-span p-0 mb-2"><span>TAGGED BRANDS</span></div>
        <div className="mb-2">
          {
            displayMentions.map((mention) => (
              <a key={mention.brandsid} href="/" className="no-style">
                <div className="profile-timeline-tag mr-0 d-flex align-items-center prestigio-shadow">
                  <div
                    className="prestigio-thumbnail mr-2"
                    style={{ backgroundImage: `url(${mention.brand[0].brand_logo_url})` }}
                  />
                  <p className="mb-0 small-text ellipsis">{mention.brand[0].brand_name}</p>
                </div>
              </a>
            ))
          }
          {addonMentionsCount > 0 && (
            <div
              onClick={() => showModal("brandsModalVisible", mentions)}
              className="profile-timeline-tag mr-0 mb-0 d-flex align-items-center prestigio-shadow"
              data-toggle="modal"
              data-target="#moreTagsModal"
            >
              <div className="prestigio-thumbnail mr-2" style={{ background: "#eee" }}>
                <div className="thumbnail-number">
                  +
                  {addonMentionsCount}
                </div>
              </div>
              <p className="mb-0 small-text ellipsis">
                +
                {addonMentionsCount}
                more tags
              </p>
            </div>
          )}
        </div>
      </>
    );
  };

  convertIndicator = (indicator) => (indicator >= 1000
    ? `${(indicator / 1000).toFixed(0)}k`
    : indicator
  );

  render() {
    const { data: { user, mentions, social }, data, showModal } = this.props;
    const { [social]: socNetworkObject } = data;
    return (
      <div className="col col-12 col-md-5">
        <div className="prestigio-pane px-3 pt-3 mb-3 prestigio-shadow d-flex flex-column justify-content-center">
          <a href="/" className="no-style d-none d-md-block">
            <div className="d-flex align-items-center mb-3">
              <div className="prestigio-thumbnail mr-2" style={{ backgroundImage: `url(${user[0].userinfo.avatar_url})` }} />
              <p className="small-text mb-0 ellipsis">{user[0].userinfo.displayname}</p>
            </div>
          </a>
          <div className="small-title-span p-0 mb-1"><span>POST METRICS</span></div>
          <div className="row pr-close-row">
            <div className="col mb-3">
              <span className="small-text op-67"><strong>Reach</strong></span>
              <h3 className="mb-0">{this.convertIndicator(socNetworkObject[0].accountmetrics.reach)}</h3>
            </div>
            <div className="col mb-3">
              <span className="small-text op-67"><strong>Engagement</strong></span>
              <h3 className="mb-0">{this.convertIndicator(socNetworkObject[0].accountmetrics.engagement)}</h3>
            </div>
          </div>
          {mentions.length > 0 && this.taggedBrands(mentions)}
        </div>
        <div className="text-right">
          <button onClick={() => showModal("shareModalVisible")} type="button" className="prestigio-btn prestigio-blue-white prestigio-shadow" data-toggle="modal" data-target="#shareModal">
            <i className="pr-icon-share" />
            {" SHARE"}
          </button>
        </div>
      </div>
    );
  }
}

export default Metrics;
