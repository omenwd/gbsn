import React, { Component } from "react";
import classNames from "classnames";

import Post from "./Post";
import Metrics from "./Metrics";

class Item extends Component {
  state = {};

  render() {
    const { index, data, showModal } = this.props;
    const isFirst = index === 0;
    return (
      <>
        <div className={classNames("mention-row", !isFirst && "mb-3")}>
          <div className="mention-body">
            <div className="row pr-close-row">
              <Post data={data} />
              <Metrics data={data} showModal={showModal} />
            </div>
          </div>
        </div>
        <div className="row pr-close-row">
          <div className="col col-8 offset-2 offset-lg-3">
            <hr />
          </div>
        </div>
      </>
    );
  }
}

export default Item;
