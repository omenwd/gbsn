import React from "react";
import classNames from "classnames";

export const SocialFilterComponent = ({
  socialFilterChange,
  selectedButton,
}) => (
  <div className="prestigio-responsive-central-pane">
    <div className="row pr-close-row">
      <div className="col col-auto mb-3">
        <button
          onClick={() => socialFilterChange("all")}
          type="button"
          className={
            classNames(
              "prestigio-btn prestigio-blue-white-inverse metrics-picker prestigio-shadow",
              selectedButton === "all" && "selected",
            )
          }
        >
          ALL
        </button>
      </div>
      <div className="col col-auto mb-3">
        <button
          onClick={() => socialFilterChange("facebook")}
          type="button"
          className={
            classNames(
              "prestigio-btn square metrics-picker prestigio-shadow facebook",
              selectedButton === "facebook" && "selected",
            )
          }
        >
          <i className="pr-icon-facebook" />
        </button>
      </div>
      <div className="col col-auto mb-3">
        <button
          onClick={() => socialFilterChange("instagram")}
          type="button"
          className={
            classNames(
              "prestigio-btn square metrics-picker prestigio-shadow instagram",
              selectedButton === "instagram" && "selected",
            )
          }
        >
          <i className="pr-icon-instagram" />
        </button>
      </div>
    </div>
  </div>
);

export default SocialFilterComponent;
