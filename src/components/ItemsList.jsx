import React, { Component } from "react";
import { uniqBy } from "lodash";

import Item from "./Item/Item";
import TopMentories from "./TopMentories";
import { BrandsModal, ShareModal } from "./Modals";

class ItemsList extends Component {
  state = {
    brandsModalVisible: false,
    shareModalVisible: false,
    mentionsList: [],
  };

  setMentoriesList = (mentions) => {
    this.setState({
      mentionsList: mentions,
    });
  };

  /**
   * @param modalType
   * @param data - mentions list for BrandsModal
   */
  showModal = (modalType, data = []) => {
    this.setState({
      [modalType]: true,
    });
    if (modalType === "brandsModalVisible") this.setMentoriesList(data);
  };

  hideModal = (modalType) => {
    this.setState({
      [modalType]: false,
    });
  };

  render() {
    const { data } = this.props;
    const {
      brandsModalVisible,
      mentionsList,
      shareModalVisible,
    } = this.state;

    const displayData = uniqBy(data, "id"); // there are come doubles from API when sort field is user name
    return (
      <div className="prestigio-tab-pane" style={{ display: "block" }} id="brand-pane" data-tabs="mentions">
        <div className="prestigio-offset-pane-big" id="content">
          <div className="row pr-responsive-row">
            <div className="col col-12 col-lg-8">
              <div className="mention-container">
                {displayData.length > 0 && (
                  displayData.map((item, index) => (
                    <Item
                      key={item.id}
                      data={item}
                      index={index}
                      showModal={this.showModal}
                      hideModal={this.hideModal}
                    />
                  ))
                )}
              </div>
            </div>
            <TopMentories />
            <BrandsModal
              visible={brandsModalVisible}
              mentionsList={mentionsList}
              hideModal={this.hideModal}
            />
            <ShareModal
              visible={shareModalVisible}
              mentionsList={mentionsList}
              hideModal={this.hideModal}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default ItemsList;
