import React, { Component } from "react";
import { connect } from "react-redux";
import { isEqual, cloneDeep } from "lodash";
import BottomScrollListener from "react-bottom-scroll-listener";

import { actions as CreatorSocialPostActions } from "../redux/reducers/creatorSocialPost.resource";

import FilterComponent from "./FilterComponent";
import SocialFilterComponent from "./SocialFilterComponent";
import ItemsList from "./ItemsList";

class Dashboard extends Component {
  state = {
    page: 0,
    limit: 4,
    data: [],
    search: "",
    sortField: "date",
    socialFilterOption: "all",
    searchUser: false,
  };

  componentDidMount() {
    this.getNewData();
  }

  componentDidUpdate(prevProps, prevState) {
    const { creatorSocialPostsArray, isCreatorSocialPostsFetching } = this.props;
    if (this.isSortFilterChanged(prevState)) {
      this.getNewData();
      return;
    }
    const contentContainer = document.getElementById("content");
    if (
      contentContainer
      && !isCreatorSocialPostsFetching
      && isEqual(prevProps.creatorSocialPostsArray, creatorSocialPostsArray)
      && contentContainer.scrollHeight > 0
      && contentContainer.scrollHeight
      <= ((window.screen.availHeight - 280) / window.devicePixelRatio)
    ) {
      this.getNewData();
      return;
    }
    if (!isEqual(prevProps.creatorSocialPostsArray, creatorSocialPostsArray)) {
      this.setDisplayData(creatorSocialPostsArray);
    }
  }

  isSortFilterChanged = (prevState) => {
    const {
      sortField, socialFilterOption, searchUser,
    } = this.state;
    return (!isEqual(prevState.sortField, sortField)
    || !isEqual(prevState.socialFilterOption, socialFilterOption)
    || !isEqual(prevState.searchUser, searchUser));
  };

  setDisplayData = (creatorSocialPostsArray) => {
    const { data, page } = this.state;
    if (page === 1) {
      this.setState({
        data: creatorSocialPostsArray,
      });
      return;
    }
    let newData = cloneDeep(data);
    newData = newData.concat(creatorSocialPostsArray);
    this.setState({
      data: newData,
    });
  };

  getNewData = () => {
    const {
      fetchCreatorSocialPosts,
      creatorSocialPostsPagination,
    } = this.props;
    if (
      creatorSocialPostsPagination
      && creatorSocialPostsPagination.totalPages < this.nextPage()
    ) {
      return;
    }
    fetchCreatorSocialPosts({}, { ...this.getQuery() });
    this.setState({
      page: this.nextPage(),
    });
  };

  /**
   * next page helper
   * @returns {number}
   */
  nextPage = () => {
    const { page } = this.state;
    return page + 1;
  };

  /**
   * make querry helper
   * @returns {{query: *}}
   */
  getQuery = () => {
    const {
      limit, socialFilterOption, sortField, search,
    } = this.state;
    const query = {};
    query.page = this.nextPage();
    query.limit = limit;
    if (sortField === "date") {
      query.sortField = "post_created_at";
      query.sortOrder = "desc";
    } else {
      query.sortField = "user.userinfo.displayname";
      query.sortOrder = "asc";
    }
    query.includes = "mentions.brand,user,social";
    if (socialFilterOption !== "all") {
      query.qField = "social";
      query.qValue = socialFilterOption;
    }
    if (search !== "") { // search by User diaplay name
      query.qField = "user.userinfo.displayname";
      query.qValue = search;
    }
    return { query };
  };

  /**
   * searchChange handle
   * @param newSearch
   */
  searchChange = (newSearch) => {
    const { search } = this.state;
    this.setState({
      search: newSearch,
    });
    if (!isEqual(search, newSearch) && newSearch === "") {
      this.processSearch();
    }
  };

  /**
   * search button click
   */
  processSearch = () => {
    const { searchUser } = this.state;
    this.setState({
      searchUser: !searchUser,
      socialFilterOption: "all",
      page: 0,
    });
  };

  /**
   * sortFieldChange handle
   * @param newSortField
   */
  sortFieldChange = (newSortField) => {
    const { sortField } = this.state;
    if (!isEqual(newSortField, sortField)) {
      this.setState({
        sortField: newSortField,
        page: 0,
      });
    }
  };

  /**
   * socialFilterChange handle
   * @param newSocialFilterOption
   */
  socialFilterChange = (newSocialFilterOption) => {
    const { socialFilterOption } = this.state;
    if (!isEqual(newSocialFilterOption, socialFilterOption)) {
      this.setState({
        socialFilterOption: newSocialFilterOption,
        search: "",
        page: 0,
      });
    }
  };

  render() {
    const { data, socialFilterOption, search } = this.state;
    return (
      <>
        <BottomScrollListener onBottom={this.getNewData} />
        <div className="container-fluid prestigio-brand-background">
          <div className="row pr-close-row">
            <div className="col col-lg-6 offset-lg-3">
              <div className="row no-gutters">
                <div
                  className="col col-10 col-lg-10 col-md-8 col-sm-8 offset-1 offset-lg-1 offset-md-2 offset-sm-2 prestigio-white-stripe prestigio-shadow"
                >
                  <FilterComponent
                    search={search}
                    searchChange={this.searchChange}
                    sortFieldChange={this.sortFieldChange}
                    processSearch={this.processSearch}
                  />
                  <SocialFilterComponent
                    selectedButton={socialFilterOption}
                    socialFilterChange={this.socialFilterChange}
                  />
                  <ItemsList data={data} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isCreatorSocialPostsFetching: state.reducerCreatorSocialPost.isFetching,
  creatorSocialPostsArray: state.reducerCreatorSocialPost.items,
  creatorSocialPostsPagination: state.pagination.creatorSocialPost,
});

export default connect(
  mapStateToProps,
  {
    ...CreatorSocialPostActions,
  },
)(Dashboard);
