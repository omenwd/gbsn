import React, { Component } from "react";

class ShareModal extends Component {
  state = {};

  render() {
    const { visible, hideModal } = this.props;
    const style = {
      display: visible ? "block" : "none",
      opacity: 1,
    };
    return (
      <div className="modal fade" id="shareModal" tabIndex="-1" role="dialog" style={style}>
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header align-items-center prestigio-blue-white prestigio-shadow no-shrink">
              SHARE MENTION
              <button
                onClick={() => hideModal("shareModalVisible")}
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body p-0">
              <div className="prestigio-modal-body">
                <div className="small-title-span p-0 mb-2">
                  <span>COPY SHARE LINK</span>
                </div>
                <div className="d-flex align-items-center justify-content-center mb-3">
                  <input
                    type="text"
                    className="prestigio-new-input text-center w-75"
                    value="https://influencer.prestigi.io/mention/a3FhS93"
                    readOnly
                  />
                  <button
                    type="button"
                    className="prestigio-btn square prestigio-blue-white-inverse prestigio-shadow ml-3"
                    id="mentions-copy-link"
                  >
                    <i className="pr-icon-clipboard" />
                  </button>
                </div>
                <div className="small-title-span p-0 mb-2">
                  <span>OR SHARE TO A SOCIAL NETWORK</span>
                </div>
                <div className="d-flex align-items-center justify-content-center my-2 flex-wrap">
                  <button type="button" className="schedule-post-sn-btn facebook">
                    <i className="pr-icon-facebook" />
                  </button>
                  <button type="button" className="schedule-post-sn-btn twitter">
                    <i className="pr-icon-twitter" />
                  </button>
                  <button type="button" className="schedule-post-sn-btn instagram">
                    <i className="pr-icon-instagram" />
                  </button>
                  <button type="button" className="schedule-post-sn-btn pinterest">
                    <i className="pr-icon-pinterest" />
                  </button>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                onClick={() => hideModal("shareModalVisible")}
                type="button"
                className="prestigio-btn prestigio-blue-white-inverse prestigio-shadow"
                data-dismiss="modal"
              >
                <i className="pr-icon-times" />
                CLOSE
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ShareModal;
