import BrandsModal from "./BrandsModal";
import ShareModal from "./ShareModal";

export {
  BrandsModal,
  ShareModal,
};
