import React, { Component } from "react";

class BrandsModal extends Component {
  state = {};

  render() {
    const { mentionsList, visible, hideModal } = this.props;
    const style = {
      display: visible ? "block" : "none",
      opacity: 1,
    };
    return (
      <div className="modal fade" id="moreTagsModal" tabIndex="-1" role="dialog" style={style}>
        <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header align-items-center prestigio-blue-white prestigio-shadow no-shrink">
              TAGGED BRANDS
              <button
                type="button"
                onClick={() => hideModal("brandsModalVisible")}
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body p-0">
              <div className="prestigio-modal-body">
                {
                  mentionsList.length > 0 && mentionsList.map((mention) => (
                    <a key={mention.brandsid} href="/" className="no-style">
                      <div className="profile-timeline-tag mr-0 d-flex align-items-center prestigio-shadow">
                        <div
                          className="prestigio-thumbnail mr-2"
                          style={{ backgroundImage: `url(${mention.brand[0].brand_logo_url})` }}
                        />
                        <p className="mb-0 small-text ellipsis">{mention.brand[0].brand_name}</p>
                      </div>
                    </a>
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BrandsModal;
