import React from "react";

export const FilterComponent = ({
  searchChange,
  sortFieldChange,
  processSearch,
  search,
}) => (
  <div className="prestigio-responsive-central-pane">
    <div className="row pr-close-row">
      <div className="col col-12 col-md-4 mb-2 mb-md-3">
        <select
          className="prestigio-new-input w-100 pr-select"
          onChange={(e) => sortFieldChange(e.target.value)}
        >
          <option value="date">Date</option>
          <option value="name">Name</option>
        </select>
      </div>
      <div className="col col-12 col-md-auto ml-auto mb-3">
        <div className="prestigio-search">
          <input
            value={search}
            type="search"
            className="prestigio-new-input w-100"
            placeholder="Search..."
            onKeyPress={(e) => { if (e.key === "Enter") processSearch(); }}
            onChange={(e) => searchChange(e.target.value)}
          />
          <button type="button" className="prestigio-search-input-btn" onClick={processSearch}>
            <i className="pr-icon-search" />
          </button>
        </div>
      </div>
    </div>
  </div>
);

export default FilterComponent;
