import React, { Component } from "react";
import { connect } from "react-redux";
import { isEqual } from "lodash";

import { actions as MentionActions } from "../redux/reducers/mention.resource";

class TopMentories extends Component {
  state = {
    topMentioners: [],
  };

  componentDidMount() {
    const { fetchMentions } = this.props;
    fetchMentions({}, { ...this.getQuery() });
  }

  componentDidUpdate(prevProps) {
    const { mentionsArray } = this.props;
    const topMentioners = [];
    if (this.isEnoughData(prevProps)) {
      mentionsArray.forEach((mention) => {
        const index = topMentioners.findIndex(({ siduser }) => siduser === mention.usersid);
        if (index === -1) {
          topMentioners.push({ ...mention.user[0], mentionsCount: 1 });
        } else {
          topMentioners[index].mentionsCount += 1;
        }
      });
      topMentioners.sort((a, b) => (a.mentionsCount > b.mentionsCount ? -1 : 1));
      topMentioners.splice(5, topMentioners.length - 5);
      this.setState({
        topMentioners,
      });
    }
  }

  getQuery = () => {
    const query = {};
    query.includes = "user";
    return { query };
  };

  isEnoughData = (prevProps) => {
    const { mentionsArray } = this.props;
    return (!isEqual(prevProps.mentionsArray, mentionsArray) && mentionsArray.length > 0);
  };

  render() {
    const { topMentioners } = this.state;
    return (
      <div className="col col-lg-4 d-none d-lg-block">
        <div className="mentions-right-column">
          <div className="prestigio-pane mb-3 prestigio-shadow">
            <h4 className="px-2 pt-2">Top mentioners</h4>
            <div className="mentions-mentioner-group">
              {topMentioners.length > 0 && topMentioners.map((mentioner, index) => (
                <a key={mentioner.siduser} href="/" className="no-style mentions-mentioner d-flex align-items-center">
                  <div className="mentions-mentioner-place">{index + 1}</div>
                  <div
                    className="prestigio-thumbnail mentions-mentioner-thumb mr-2"
                    style={{ backgroundImage: `url(${mentioner.userinfo.avatar_url})` }}
                  />
                  <p className="small-text mb-0 ellipsis">{mentioner.userinfo.displayname}</p>
                  <p className="small-text op-67 mb-0 ml-auto mr-1">{mentioner.mentionsCount}</p>
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  mentionsArray: state.reducerMention.items,
});

export default connect(
  mapStateToProps,
  {
    ...MentionActions,
  },
)(TopMentories);
