import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../redux/reducers';

const reducer = combineReducers({
  ...reducers,
});
const store = createStore(reducer, compose(applyMiddleware(thunk)));

export { store };
