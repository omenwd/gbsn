import { createResource } from 'redux-rest-resource';
import { apiHost } from "../config";

export const {types, actions, rootReducer} = createResource({
  name: 'creatorSocialPost',
  url: `${apiHost}/creator_social_posts`,
  actions: {
    fetch: {
      method: "GET",
      transformResponse: res => ({
        body: res.body.data.attributes,
        pagination: {
          resourceName: "creatorSocialPost",
          totalResults: res.body.meta.totalResults,
          totalPages: res.body.meta.totalPages || false,
          totalPerPage: res.body.meta.totalPerPage || false,
        },
      }),
    },
  },
});