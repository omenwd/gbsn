import { createResource } from "redux-rest-resource";
import { apiHost } from "../config";

export const { types, actions, rootReducer } = createResource({
  name: "mention",
  url: `${apiHost}/mentions`,
  actions: {
    fetch: {
      method: "GET",
      transformResponse: (res) => ({
        body: res.body.data.attributes,
        pagination: {
          resourceName: "mention",
          totalResults: res.body.meta.totalResults,
          totalPages: res.body.meta.totalPages || false,
          totalPerPage: res.body.meta.totalPerPage || false,
        },
      }),
    },
  },
});
