import { rootReducer as reducerCreatorSocialPost } from "./reducers/creatorSocialPost.resource";
import { rootReducer as reducerMention } from "./reducers/mention.resource";
import pagination from "./reducers/pagination.reducer";

export default {
  reducerCreatorSocialPost,
  reducerMention,
  pagination,
};
